# IOC Factory

The IOC Factory (FACT) is responsible for managing IOCs at the European Spallation Source (ESS). It is designed to
maximize the Integrated Control System (ICS) division�s productivity by providing a consistent and automated approach
on how hundreds of IOCs are configured, generated, browsed and audited which would otherwise have to be performed
manually. This system is part of the Work Unit 3.1 referring to the Controls Configuration Data Management which
addresses the collection, storage, and distribution of static data needed to install, commission, and operate the
control system. By managing, it specifically means the following use cases:

   - Configure IOC
   - Generate IOC
   - Browse IOC
   - Audit IOC

The "Configure IOC" allows the user to select a set of EPICS modules for a certain IOC to use to effectively interface
its devices. This selection/configuration is stored in a persistence layer and it can later be used to generate the IOC.
In principle, every time that the topology of the IOC evolves (i.e. the layout of devices that the IOC interface
changes) the user creates a new configuration to cope with this evolution.

The "Generate IOC" allows the generation (i.e. building) of an IOC from scratch according to a certain configuration
selected by the user. The generated IOC is stored in the official repository server for development or production,
depending on what the user has selected. The information about the generation is stored in a persistence layer to enable
the browse and audit IOC use cases.

The "Browse IOC" allows the user to retrieve, organize and display information about historical (i.e. past) generation
of IOCs. It gives a broad view/understanding of when, how and why a certain IOC was generated in the official repository
server and by whom.

Finally, the "Audit IOC" enables the user to track the changes that an IOC (stored in the official repository server)
may have suffered. In other words, it provides him/her with a complete list of files/directories belonging to the IOC
that have been added, modified or removed.

## Docker

The application can be built and run as a Docker container. The image is based on the jboss/wildfly image from Docker Hub.

### How to use this image

```
$ docker run registry.esss.lu.se/ics-software/ioc-factory
```

Environment variables that can be set when running a container based on this image:

| Environment variable     | Default    | Description |
| -------------------------|------------|-------------|
| IOCFACTORY_DEPLOYMENT_CONTEXT_ROOT | / | Context root used for the web application |
| IOCFACTORY_DATABASE_NAME | iocfactory | Database name used for the database connection |
| IOCFACTORY_DATABASE_HOST | iocfactory-postgres | Host used for the database connection |
| IOCFACTORY_DATABASE_PORT | 5432 | Port used for the database connection |
| IOCFACTORY_DATABASE_USERNAME | iocfactory | Username used for the database connection |
| IOCFACTORY_DATABASE_PASSWORD | iocfactory | Password used for the database connection |
| IOCFACTORY_NAMING_URL | https://naming.esss.lu.se/ | URL for Naming web application |
| IOCFACTORY_CCDB_URL | https://ccdb.esss.lu.se/ | URL for CCDB web application |
| IOCFACTORY_CCDB_REST_URL | https://ccdb.esss.lu.se/ | URL for CCDB REST services |
| RBAC_PRIMARY_URL | https://rbac.esss.lu.se:8443/service | URL for primary RBAC service |
| RBAC_PRIMARY_SSL_HOST | rbac.esss.lu.se | SSL host for primary RBAC service |
| RBAC_PRIMARY_SSL_PORT | 8443 | SSL port for primary RBAC service |
| RBAC_SINGLE_SIGNON | false | Use single sign-on |
| RBAC_SECONDARY_URL | https://localhost:8443/service | URL for secondary RBAC service |
| RBAC_SECONDARY_SSL_HOST | localhost | SSL host for secondary RBAC service |
| RBAC_SECONDARY_SSL_PORT | 8443 | SSL port for secondary RBAC service |
| RBAC_HANDSHAKE | true | Perform SSL handshake with RBAC |
| RBAC_HANDSHAKE_TIMEOUT | 2000 | Timeout for SSL handshake with RBAC |
| RBAC_INACTIVITY_TIMEOUT_DEFAULT | 900 | Inactivity timeout for RBAC client |
| RBAC_INACTIVITY_RESPONSE_GRACE | 30 | Inactivity response grace period for RBAC client |
| RBAC_SHOW_ROLE_SELECTOR | false | Show role selector in RBAC client |
| RBAC_VERIFY_SIGNATURE | false | Verify signature of RBAC |
| RBAC_PUBLIC_KEY_LOCATION | ~/.rbac/rbac.key | Public key for RBAC |
| RBAC_LOCAL_SERVICES_PORT | 9421 | Port for local RBAC service |
| RBAC_USE_LOCAL_SERVICE | false | Use local RBAC service |
| RBAC_CERTIFICATE_STORE | ~/.rbac | SSL certificate store for RBAC client |
| RBAC_COOKIE_DOMAIN | .esss.lu.se | RBAC Cookie domain |
| LDAP_PRIMARY_HOST | dc01.esss.lu.se | Primary LDAP host |
| LDAP_PRIMARY_PORT | 389 | Primary LDAP port |
| LDAP_SECONDARY_HOST | dc02.esss.lu.se | Secondary LDAP host |
| LDAP_SECONDARY_PORT | 389 | Secondary LDAP port |
| LDAP_TIMEOUT | 7000 | Timeout for LDAP connections |
| LDAP_SECURITY_PRINCIPAL | CN=ldapreadonly,CN=Users,DC=esss,DC=lu,DC=se | Bind DN for LDAP connections |
| LDAP_SECURITY_CREDENTIALS |  | Bind password for LDAP connections |
| LDAP_SEARCH_NAME | DC=esss,DC=lu,DC=se | LDAP user search base |
| LDAP_SEARCH_FILTER_USERNAME | (sAMAccountName={0}) | LDAP username search filter |
| LDAP_SEARCH_FILTER_FIRSTNAME | (givenName={0}) | LDAP firstname search filter |
| LDAP_SEARCH_FILTER_LASTNAME | (sn={0}) | LDAP lastname search filter |
| LDAP_SEARCH_FILTER_OBJECT | (objectCategory=CN=Person,CN=Schema,CN=Configuration,DC=esss,DC=lu,DC=se)(objectClass=user) | LDAP user search filter |
| LDAP_SEARCH_PAGESIZE | 500 | LDAP page size |
| LDAP_ATTR_USERNAME | samaccountname | LDAP username attribute |
| LDAP_ATTR_FIRSTNAME | givenname | LDAP firstname attribute |
| LDAP_ATTR_MIDDLENAME |  | LDAP middlename attribute |
| LDAP_ATTR_LASTNAME | sn | LDAP lastname attribute |
| LDAP_ATTR_GROUP | department | LDAP group attribute |
| LDAP_ATTR_GROUP_FILTER | OU=ESS Organisational Units,DC=esss,DC=lu,DC=se | LDAP group filter |
| LDAP_ATTR_EMAIL | mail | LDAP email attribute |
| LDAP_ATTR_PHONE | telephonenumber | LDAP phone attribute |
| LDAP_ATTR_MOBILE | mobile | LDAP mobile attribute |
| LDAP_ATTR_LOCATION |  | LDAP location attribute |


### Docker Compose

For convenience, the application comes with a docker-compose.yml file, which can be used to run the application with required services and configuration:

```
$ docker-compose up
```
