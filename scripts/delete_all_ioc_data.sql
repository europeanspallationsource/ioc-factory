/* Search replace the IOC name 'LEBT-00:Ctrl-IOC-1' to the correct one */

delete from device_parameter where device_configuration_id in (select id from device_configuration where configuration_id IN (select id from configuration where ioc_name = 'LEBT-00:Ctrl-IOC-1'));
delete from device_configuration where configuration_id IN (select id from configuration where ioc_name = 'LEBT-00:Ctrl-IOC-1');
delete from device_parameter where configuration_id IN (select id from configuration where ioc_name = 'LEBT-00:Ctrl-IOC-1');
delete from configuration where ioc_name = 'LEBT-00:Ctrl-IOC-1';
delete from ioc where ioc_name = 'LEBT-00:Ctrl-IOC-1';
delete from generate_entry where ioc_name = 'LEBT-00:Ctrl-IOC-1';
