#!/bin/bash

cd /opt
# Copy all empty directories
find ./epics -type d -print0 | tar cf ~/dirstructure.tar.gz  --null --files-from - --no-recursion
# Copy all relevant dep and cmd files
find ./epics -name "*.cmd" -o -name "*.dep" | tar -cf ~/eee.tar.gz -T -
