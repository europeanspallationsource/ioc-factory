<#if licenseFirst??>
${licenseFirst}
</#if>
${licensePrefix}Copyright (c) 2015-2016 European Spallation Source
${licensePrefix}Copyright (c) 2015-2016 Cosylab d.d.
${licensePrefix}
${licensePrefix}This file is part of IOC Factory.
${licensePrefix}
${licensePrefix}IOC Factory is free software: you can redistribute it
${licensePrefix}and/or modify it under the terms of the GNU General Public License as
${licensePrefix}published by the Free Software Foundation, either version 2 of the License,
${licensePrefix}or any newer version.
${licensePrefix}
${licensePrefix}This program is distributed in the hope that it will be useful, but WITHOUT
${licensePrefix}ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
${licensePrefix}FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
${licensePrefix}details.
${licensePrefix}
${licensePrefix}You should have received a copy of the GNU General Public License along with
${licensePrefix}this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
<#if licenseLast??>
${licenseLast}
</#if>
