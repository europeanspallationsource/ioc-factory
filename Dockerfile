FROM jboss/wildfly:8.2.1.Final
LABEL maintainer="anders.harrisson@esss.se"

# PostgreSQL jdbc driver module
ENV DATABASE_DRIVER_VERSION 42.2.0
ENV DATABASE_DRIVER postgresql-${DATABASE_DRIVER_VERSION}.jar
ADD --chown=jboss:jboss https://artifactory.esss.lu.se/artifactory/repo1/org/postgresql/postgresql/${DATABASE_DRIVER_VERSION}/${DATABASE_DRIVER} /opt/jboss/wildfly/standalone/deployments/

# GELF logging module
ADD --chown=jboss:jboss https://artifactory.esss.lu.se/artifactory/repo1/biz/paluch/logging/logstash-gelf/1.12.0/logstash-gelf-1.12.0-logging-module.zip /tmp/gelf-logging-module.zip
RUN unzip -q /tmp/gelf-logging-module.zip -d /tmp/ && mv /tmp/logstash-gelf-*/* /opt/jboss/wildfly/modules/ && rmdir /tmp/logstash-gelf-* && rm /tmp/gelf-logging-module.zip

# Standalone configuration
COPY --chown=jboss:jboss standalone.xml /opt/jboss/wildfly/standalone/configuration/

# Deployment unit
COPY target/iocfactory-*.war /opt/jboss/wildfly/standalone/deployments/
