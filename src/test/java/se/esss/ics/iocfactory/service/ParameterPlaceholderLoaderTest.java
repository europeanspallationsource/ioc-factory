/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.apache.commons.io.IOUtils;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import se.esss.ics.iocfactory.model.ParameterPlaceholder;
import se.esss.ics.iocfactory.model.ParameterType;
import se.esss.ics.iocfactory.model.Repository;

import static se.esss.ics.iocfactory.model.PlaceholderTrait.*;

public class ParameterPlaceholderLoaderTest {

    public static final String RESOURCES_FOLDER = "/se.esss.ics.iocfactory.service/";

    public static final String PREFIX = "temporary.st";
    public static final String SUFFIX = ".cmd";

    @Test
    public void testAllTypes() throws IOException {
        final File testFile = getResourceAsTemporaryFile("st.cmd.alltypes");

        try {
            final ParameterPlaceholderLoader loader = new ParameterPlaceholderLoader(null, testFile);
            final List<ParameterPlaceholder> result = loader.loadParameterPlaceholders();

            assertTrue(result.get(0).getTraits().contains(GLOBAL_HINT));
            assertFalse(result.get(0).getTraits().contains(RUNTIME));
            assertFalse(result.get(0).getTraits().contains(UNIQUE_IOC));
            assertFalse(result.get(0).getTraits().contains(UNIQUE_MACHINE));
            assertEquals("SOME_BUFFER_SIZE", result.get(0).getName());
            assertEquals(ParameterType.INTEGER, result.get(0).getType());
            
            assertFalse(result.get(1).getTraits().contains(GLOBAL_HINT));
            assertTrue(result.get(1).getTraits().contains(RUNTIME));
            assertFalse(result.get(1).getTraits().contains(UNIQUE_IOC));
            assertFalse(result.get(1).getTraits().contains(UNIQUE_MACHINE));
            assertEquals("SOME_DOUBLE", result.get(1).getName());
            assertEquals(ParameterType.DOUBLE, result.get(1).getType());
                        
            assertFalse(result.get(2).getTraits().contains(GLOBAL_HINT));
            assertFalse(result.get(2).getTraits().contains(RUNTIME));
            assertTrue(result.get(2).getTraits().contains(UNIQUE_IOC));
            assertFalse(result.get(2).getTraits().contains(UNIQUE_MACHINE));
            assertEquals("UNIIOC", result.get(2).getName());
            assertEquals(ParameterType.STRING, result.get(2).getType());
            
            assertFalse(result.get(3).getTraits().contains(GLOBAL_HINT));
            assertFalse(result.get(3).getTraits().contains(RUNTIME));
            assertFalse(result.get(3).getTraits().contains(UNIQUE_IOC));
            assertTrue(result.get(3).getTraits().contains(UNIQUE_MACHINE));
            assertEquals("UNIMACHINE", result.get(3).getName());
            assertEquals(ParameterType.STRING, result.get(3).getType());
                        
            
        }  finally {
            testFile.delete();
        }
    }
    
    
    /** Tests loading a file with all possible placeholder formats. */
    @Test
    public void testLoadParameterplaceholders() throws IOException {

        final File testFile = getResourceAsTemporaryFile("st.cmd.valid");

        try {
            final ParameterPlaceholderLoader loader = new ParameterPlaceholderLoader(null, testFile);
            final List<ParameterPlaceholder> result = loader.loadParameterPlaceholders();
            assertNotNull(result);
            assertFalse(result.isEmpty());
            assertEquals(5, result.size());

            final ParameterPlaceholder parameterPlaceholder1 = result.get(0);
            assertEquals("NELM", parameterPlaceholder1.getName());
            assertEquals(ParameterType.INTEGER, parameterPlaceholder1.getType());
            assertEquals("Size of the waveform produced by the DAQ.", parameterPlaceholder1.getDesc());

            final ParameterPlaceholder parameterPlaceholder2 = result.get(1);
            assertEquals("TSEL", parameterPlaceholder2.getName());
                assertEquals(ParameterType.LINK, parameterPlaceholder2.getType());
            assertEquals("Link to the timing receiver \"action\" that is providing timestamps.",
                    parameterPlaceholder2.getDesc());

            final ParameterPlaceholder parameterPlaceholder3 = result.get(2);
            assertEquals("EVNT", parameterPlaceholder3.getName());
            assertEquals(ParameterType.INTEGER, parameterPlaceholder3.getType());
            assertEquals("EPICS user event ID that triggers processing of the DAQ.\n\n"
                    + "Must match the timing receiver's event ID.",
                    parameterPlaceholder3.getDesc());

            final ParameterPlaceholder parameterPlaceholder4 = result.get(3);
            assertEquals("SIM", parameterPlaceholder4.getName());
            assertEquals(ParameterType.STRING, parameterPlaceholder4.getType());
            assertEquals("Simulated waveform. An array of doubles whose size should match SIM_NELM.",
                    parameterPlaceholder4.getDesc());

        } finally {
            testFile.delete();
        }
    }

    /** Tests that loading empty file produces empty result. */
    @Test
    public void testLoadEmptyFile() throws IOException {
        final ParameterPlaceholderLoader loader = new ParameterPlaceholderLoader(null, new File("deijd93jdd,.emcic"));
        final List<ParameterPlaceholder> result = loader.loadParameterPlaceholders();
        assertTrue(result.isEmpty());
    }

    /** Test that file with a parameter with invalid type cannot be loaded. */
    @Test
    public void testInvalidType() throws IOException {
        final File testFile = getResourceAsTemporaryFile("st.cmd.invalidtype");

        try {
            final Repository repo = new Repository();
            final ParameterPlaceholderLoader loader = new ParameterPlaceholderLoader(repo, testFile);
            loader.loadParameterPlaceholders();

            assertEquals(1, repo.getMessages().size());
            assertTrue(repo.getMessages().get(0).getDesc().startsWith("Unsupported parameter type "));
        } finally {
            testFile.delete();
        }
    }

    /** Test that file with a parameter with duplicate type definition cannot be loaded. */
    @Test
    public void testDupDef() throws IOException {
        final File testFile = getResourceAsTemporaryFile("st.cmd.dupdef");

        try {
            final Repository repo = new Repository();
            final ParameterPlaceholderLoader loader = new ParameterPlaceholderLoader(repo, testFile);
            loader.loadParameterPlaceholders();

            assertEquals(1, repo.getMessages().size());
            assertTrue(repo.getMessages().get(0).getDesc().
                    startsWith("Duplicate parameter, ignoring subsequent definition"));
        } finally {
            testFile.delete();
        }
    }

    /** Test that file with a parameter with a missing type definition cannot be loaded. */
    @Test
    public void testMissingDef() throws IOException {
        final File testFile = getResourceAsTemporaryFile("st.cmd.missingdef");

        try {
            Repository repo = new Repository();
            ParameterPlaceholderLoader loader = new ParameterPlaceholderLoader(repo, testFile);
            loader.loadParameterPlaceholders();

            assertEquals(1, repo.getMessages().size());
            assertTrue(repo.getMessages().get(0).getDesc().
                    startsWith("Parameter type not specified for parameter"));
        } finally {
            testFile.delete();
        }
    }

    private File getResourceAsTemporaryFile(String name) throws IOException {
        InputStream in = getClass().getResourceAsStream(RESOURCES_FOLDER + name);

        final File tempFile = File.createTempFile(PREFIX, SUFFIX);
        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            IOUtils.copy(in, out);
        } catch (IOException io) {
            // Return empty/incomplete file if an exception.
        }
        return tempFile;
    }
}
