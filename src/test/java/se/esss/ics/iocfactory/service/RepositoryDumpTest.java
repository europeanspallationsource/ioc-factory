/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.io.File;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.output.NullOutputStream;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.model.RepositoryStats;

public class RepositoryDumpTest {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Before
    public void init() throws URISyntaxException, ZipException {
        URL zipUrl = RepositoryScannerTest.class.getResource("/epics_base_dir_example.zip");
        ZipFile zipFile = new ZipFile(new File(zipUrl.toURI()));
        zipFile.extractAll(folder.getRoot().getAbsolutePath());
    }

    @Test
    public void testDumpRepository() {
        final RepositoryScanner scanner = new RepositoryScanner(folder.getRoot());
        final Repository repo = scanner.scanRepository(null);

        PrintWriter writer = new PrintWriter(new NullOutputStream());
        RepositoryStats repoStats = new RepositoryStats();
        RepositoryDump.dumpRepositoryStats(repo, repoStats);
        writer.flush();
    }

}
