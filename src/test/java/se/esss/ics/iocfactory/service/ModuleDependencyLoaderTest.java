package se.esss.ics.iocfactory.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import se.esss.ics.iocfactory.model.ModuleDependency;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.util.ModuleVersionsComparator;

public class ModuleDependencyLoaderTest {
    private final Repository repo = new Repository();
    private final File depFile = new File("blah.txt");

    @Test
    public void testParseDependencyWithGreater() {

        ModuleDependency someDep = ModuleDependencyLoader.parseDependency(repo, depFile, "asyn,4.21+", 0);

        assertNotNull(someDep);
        assertEquals("asyn", someDep.getName());
        assertEquals(0, ModuleVersionsComparator.compare("4.21.0", someDep.getVersion()));
        assertEquals(true, someDep.isGreater());
    }

    @Test
    public void testParseDependencyWithoutGreater() {

        ModuleDependency someDep = ModuleDependencyLoader.parseDependency(repo, depFile, "asyn,4", 0);

        assertNotNull(someDep);
        assertEquals("asyn", someDep.getName());
        assertEquals(0, ModuleVersionsComparator.compare("4.0.0", someDep.getVersion()));
        assertEquals(false, someDep.isGreater());


        ModuleDependency someOtherDep = ModuleDependencyLoader.parseDependency(repo, depFile, "    asyn,4  ", 0);

        assertNotNull(someOtherDep);
        assertEquals("asyn", someOtherDep.getName());
        assertEquals(0, ModuleVersionsComparator.compare("4.0.0", someOtherDep.getVersion()));
        assertEquals(false, someOtherDep.isGreater());
    }

    @Test
    public void testPardeDependencyWithOnlyModule() {

        ModuleDependency someDep = ModuleDependencyLoader.parseDependency(repo, depFile, "    asyn  ", 0);

        assertNotNull(someDep);
        assertEquals("asyn", someDep.getName());
        assertEquals(0, ModuleVersionsComparator.compare("", someDep.getVersion()));
        assertEquals(false, someDep.isGreater());
    }

    @Test
    public void testParseDependencyComment() {
        assertNull(ModuleDependencyLoader.parseDependency(repo, depFile, "#asyn,4", 0));
        assertNull(ModuleDependencyLoader.parseDependency(repo, depFile, "   #asyn,4", 0));
        assertNull(ModuleDependencyLoader.parseDependency(repo, depFile, " # araeadsadsad    ", 0));
        assertNull(ModuleDependencyLoader.parseDependency(repo, depFile, " # w e we    ", 0));
    }

    @Test
    public void loadModuleDependenciesTest() throws IOException {
        final File testFile = File.createTempFile("moduleTest", "dep");

        try (final OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(testFile),
                StandardCharsets.US_ASCII)) {
            writer.write("# Generated file. Do not edit.\n" +
                            "asyn,4.21+\n" +
                            "sis8300,1.10+\n" +
                            "sis8300drvbcm,1.0+\n" +
                            "sis8300drv,1.5+\n" +
                            "nds,2.3+\n");
        } catch (IOException io) { }

        final List<ModuleDependency> result = ModuleDependencyLoader.loadModuleDependencies(repo, testFile);
        assertFalse(result.isEmpty());

        testFile.delete();
    }

    @Test
    public void loadModuleDependenciesTestFailed() throws IOException {
        final List<ModuleDependency> result = ModuleDependencyLoader.loadModuleDependencies(repo,
                new File("deijd93jdd,.emcic"));
        assertTrue(result.isEmpty());

    }
}
