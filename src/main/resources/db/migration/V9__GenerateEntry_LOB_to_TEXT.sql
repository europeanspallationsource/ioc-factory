ALTER TABLE generate_entry RENAME dump TO dumpoid;

ALTER TABLE generate_entry ADD COLUMN dump TEXT;

UPDATE generate_entry SET dump = convert_from(loread(lo_open(dumpoid, 262144), 1000000), 'utf8');

ALTER TABLE generate_entry DROP COLUMN dumpoid;

