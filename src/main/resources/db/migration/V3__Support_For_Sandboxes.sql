ALTER TABLE iocfact_setup ADD COLUMN sandbox_base_directory CHARACTER VARYING(255);

ALTER TABLE iocenvironment ADD COLUMN eee_type CHARACTER VARYING(255);

UPDATE iocenvironment SET eee_type = 'PRODUCTION';
