/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

var dmp = new diff_match_patch();

diff_match_patch.prototype.diff_prettyHtml = function(diffs) {
    var html = [];
    var pattern_nbsp = '\s';
    var pattern_amp = /&/g;
    var pattern_lt = /</g;
    var pattern_gt = />/g;
    var pattern_para = /\n/g;
    for (var x = 0; x < diffs.length; x++) {
        var op = diffs[x][0];    // Operation (insert, delete, equal)
        var data = diffs[x][1];  // Text of change.
        var text = data.replace(pattern_amp, '&amp;').replace(pattern_lt, '&lt;')
                .replace(pattern_gt, '&gt;').replace(pattern_para, '<br/>');
        switch (op) {
            case DIFF_INSERT:
                html[x] = '<ins style="background:#e6ffe6;">' + text + '</ins>';
                break;
            case DIFF_DELETE:
                html[x] = '<del style="background:#ffe6e6;">' + text + '</del>';
                break;
            case DIFF_EQUAL:
                html[x] = '<span>' + text + '</span>';
                break;
        }
    }
    return html.join('');
};

//

/**
 * Calculates and formats a diff
 *
 * @param {type} dlgName a PrimeFaces dialog name (widgetVar)
 * @param {type} inIdA a HTML element id containing the left-hand text to be compared
 * @param {type} inIdB a HTML element id containing the right hand text to be compared
 * @param {type} outIdA a HTML div element id to which to ouput the left hand side
 * @param {type} outIdB a HTML div element id to which to ouput the right hand side
 *
 */
function showDiffDialogInternal(dlgName, inIdA, inIdB, outIdA, outIdB) {
    PF(dlgName).show();

    var text1 = document.getElementById(inIdA).value;
    var text2 = document.getElementById(inIdB).value;

    var d = dmp.diff_main(text1, text2);

    dmp.diff_cleanupSemantic(d);

    var ds = dmp.diff_prettyHtml(d);

    var pattern_nbsp = '\s';
    var pattern_para = /\n/g;

    document.getElementById(outIdA).innerHTML = text1.replace(pattern_para, '<br/>');
    document.getElementById(outIdB).innerHTML = ds;
}

function showDiffDialog() {
    showDiffDialogInternal("compareDlg", 'compareForm:confA', 'compareForm:confB',
            'compareForm:diffA', 'compareForm:diffB');
}


function showDualDiffDialog(xhr, status, args) {
    if (args.validationFailed) {
        return;
    }

    showDiffDialogInternal("dualCompareDlg",
            'dualCompareForm:envA', 'dualCompareForm:envB',
            'dualCompareForm:envAOut', 'dualCompareForm:envBOut');

    showDiffDialogInternal("dualCompareDlg",
            'dualCompareForm:confA', 'dualCompareForm:confB',
            'dualCompareForm:confAOut', 'dualCompareForm:confBOut');
}