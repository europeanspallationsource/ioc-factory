/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model.log;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;

/**
 * Settings saved env
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@JsonPropertyOrder({"name", "directory", "production", "eeeType"})
public class SettingsSavedEnvV1 {
    private final String name;
    private final String directory;
    private final boolean production;
    private final String eeeType;

    public SettingsSavedEnvV1(IOCEnvironment env) {
        this.name = env.getName();
        this.directory = env.getDirectory();
        this.production = env.isProduction();
        this.eeeType = env.getEeeType() != null ? env.getEeeType().getName() : null;
    }

    public String getName() { return name; }
    public String getDirectory() { return directory; }
    public boolean isProduction() { return production; }
    public String getEeeType() { return eeeType; }
}
