/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;


import static se.esss.ics.iocfactory.model.ConsistencyMsgLevel.ERROR;
import static se.esss.ics.iocfactory.model.ConsistencyMsgLevel.INFO;

/**
 * {@link ConsistencyStatus} enum definitions for the {@link Parameter} entity type
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public enum ParameterConsistencyStatus implements ConsistencyStatus {
    PERSISTED_INVALID("Parameter is persisted but does not exist in the module version and snippet in EEE", ERROR),
    INVALID_DEVICE_CONFIG("Invalid device configuration", ERROR),
    PARAM_IS_NEW("New parameter from EEE", INFO);

    private final String desc;
    private final ConsistencyMsgLevel level;

    private ParameterConsistencyStatus(String desc, ConsistencyMsgLevel level)
    {
        this.desc = desc;
        this.level = level;
    }

    public String getDesc() { return desc; }
    public ConsistencyMsgLevel getLevel() { return level; }
}
