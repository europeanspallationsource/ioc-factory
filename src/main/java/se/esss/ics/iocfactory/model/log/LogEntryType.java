/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model.log;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 * 
 */
public enum LogEntryType {
    CONFIG_ADDED_V1("Configuration Added"), 
    CONFIG_SAVED_V1("Configuration Saved"), 
    CONFIG_DELTED_V1("Configuration Deleted"), 
    CONFIG_GENERATED_V1("Configuration Generated"), 
    SETTINGS_SAVED_V1("Configuration Saved");

    private String description;

    LogEntryType(String desc) {
           this.description = desc;
    }

    @Override
    public String toString() {
        return this.description;
    }
    
    
}
