/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import static se.esss.ics.iocfactory.model.ConsistencyMsgLevel.ERROR;
import static se.esss.ics.iocfactory.model.ConsistencyMsgLevel.HIGHLIGHT;

/**
 * {@link ConsistencyStatus} enum definitions for {@link IOC} entity types
 * 
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public enum IOCConsistencyStatus implements ConsistencyStatus  {
    IOC_NOT_IN_CCDB("IOC does not exist in CCDB", ERROR),
    NEW_IOC("New IOC in CCDB", HIGHLIGHT),
    INVALID_CCDB_STRUCTURE("IOC in CCDB should have a single parent slot (a CPU or a VM instance)", ERROR),
    INVALID_OS("Unkown OS", ERROR);

    private final String desc;
    private final ConsistencyMsgLevel level;

    private IOCConsistencyStatus(String desc, ConsistencyMsgLevel level)
    {
        this.desc = desc;
        this.level = level;
    }

    @Override
    public String getDesc() { return desc; }

    @Override
    public ConsistencyMsgLevel getLevel() { return level; }
}
