/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model.log;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.GenerateEntry;

/**
 * Log entry that represents the action of generating a configuration
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@JsonPropertyOrder({"configDetails", "hostname", "environment", "production", "targetPath", "desc"})
public class ConfigGeneratedV1 implements Serializable {
    private final ConfigAddedEntryV1 configDetails;
    private final String hostname;
    private final String environment;
    private final boolean production;
    private final String targetPath;
    private final String desc;

    public ConfigGeneratedV1(Configuration config, GenerateEntry ge, String desc) {
        this.configDetails = new ConfigAddedEntryV1(config);
        this.hostname = ge.getHostname();
        this.environment = ge.getEnvironment();
        this.production = ge.getProduction();
        this.targetPath = ge.getTargetPath();
        this.desc = desc;
    }

    public ConfigAddedEntryV1 getConfigDetails() { return configDetails; }
    public String getHostname() { return hostname; }
    public String getEnvironment() { return environment; }
    public boolean getProduction() { return production; }
    public String getTargetPath() { return targetPath; }
    public String getDesc() { return desc; }
}
