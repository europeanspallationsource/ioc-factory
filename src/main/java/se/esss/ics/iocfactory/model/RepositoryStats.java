/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;


import org.primefaces.model.TreeNode;

import java.util.Date;

/**
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class RepositoryStats {
    private String repoHash;
    private byte[] repoDump;
    private String repoDumpStr;

    private TreeNode repoTree;
    private TreeNode selectedNode;

    private Date lastRepoScanTime;

    public RepositoryStats() {}

    public String getRepoHash() { return repoHash; }
    public void setRepoHash(final String repoHash) { this.repoHash = repoHash; }

    public byte[] getRepoDump() { return repoDump; }
    public void setRepoDump(final byte[] repoDump) { this.repoDump = repoDump; }

    public String getRepoDumpStr() { return repoDumpStr; }
    public void setRepoDumpStr(final String repoDumpStr) { this.repoDumpStr = repoDumpStr; }

    public TreeNode getRepoTree() { return repoTree; }
    public void setRepoTree(final TreeNode repoTree) { this.repoTree = repoTree; }

    public Date getLastRepoScanTime() { return lastRepoScanTime; }
    public void setLastRepoScanTime(final Date lastRepoScanTime) { this.lastRepoScanTime = lastRepoScanTime; }

    public TreeNode getSelectedNode() { return selectedNode; }
    public void setSelectedNode(final TreeNode selectedNode) { this.selectedNode = selectedNode; }
}
