/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import java.io.Serializable;
import java.util.Objects;

/**
 * A {@link ConsistencyStatus}, with additional field "Category" used to convey Configuration verification information

 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class ConfigVerifyStatus extends ConsistencyStatusValue
        implements ConsistencyStatus, Serializable {
    protected final String category;

    /**
     * Constructs a {@link ConfigVerifyStatus} of the given level, description and message category
     * @param level
     * @param desc
     * @param category
     */
    public ConfigVerifyStatus(final ConsistencyMsgLevel level, final String desc, final String category) {
        super(level, desc);
        this.category = category;
    }

    public String getCategory() { return category; }


    @Override
    public int hashCode() { return Objects.hash(this.level, this.desc, this.category); }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        final ConfigVerifyStatus other = (ConfigVerifyStatus) obj;
        return Objects.equals(this.desc, other.desc) && this.level == other.level &&
                Objects.equals(this.category, other.category);
    }
}
