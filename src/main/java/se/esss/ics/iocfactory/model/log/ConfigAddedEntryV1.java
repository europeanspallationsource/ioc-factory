/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model.log;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.Date;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.model.jsonserializers.EpicsVersionSerializer;
import se.esss.ics.iocfactory.model.jsonserializers.IOCSerializer;


/**
 * Represents an log entry for an configuration being added
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@JsonPropertyOrder({"ioc", "revision", "os", "epicsVersion", "lastEdit", "comment"})
public class ConfigAddedEntryV1 implements Serializable
{
    @JsonSerialize(using = IOCSerializer.class)
    private final IOC ioc;

    private final int revision;

    private final String os;

    @JsonProperty("description")
    private final String comment;

    private final Date lastEdit;

    @JsonSerialize(using = EpicsVersionSerializer.class)
    private Long epicsVersion;

    private String envVersion;

    public ConfigAddedEntryV1(Configuration config) {
        this.ioc = config.getIoc();
        this.revision = config.getRevision();
        this.os = config.getOs();
        this.comment = config.getComment();
        this.lastEdit = new Date(config.getLastEdit().getTime());
    }

    public IOC getIoc() { return ioc; }
    public int getRevision() { return revision; }
    public String getOs() { return os; }
    public String getComment() { return comment; }
    public Date getLastEdit() { return lastEdit; }
    public Long getEpicsVersion() { return epicsVersion; }
    public String getEnvVersion() { return envVersion; }
}