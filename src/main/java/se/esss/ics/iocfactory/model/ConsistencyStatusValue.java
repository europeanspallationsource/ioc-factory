/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import java.io.Serializable;
import java.util.Objects;

/**
 * Represents non-enum implementation of {@link ConsistencyStatus}
 *
 * Used for repository scanning messages, audit entry error messages and parameter macro extension statuses
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class ConsistencyStatusValue implements ConsistencyStatus, Serializable {
    protected final ConsistencyMsgLevel level;
    protected final String desc;

    /**
     * Constructs a {@link RepositoryMessage} of the given level and description
     * @param level
     * @param desc
     */
    public ConsistencyStatusValue(ConsistencyMsgLevel level, String desc) {
        this.level = level;
        this.desc = desc;
    }

    @Override
    public ConsistencyMsgLevel getLevel() { return level; }

    @Override
    public String getDesc() { return desc; }

    @Override
    public int hashCode() { return Objects.hash(this.level, this.desc); }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        final ConsistencyStatusValue other = (ConsistencyStatusValue) obj;
        return Objects.equals(this.desc, other.desc) && this.level == other.level;
    }
}
