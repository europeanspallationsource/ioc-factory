/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

/**
 * Macro parameter data-type defining enum
 *
 * See http://stackoverflow.com/q/3639225/3989524, specifically answer http://stackoverflow.com/a/26719904/3989524
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 *
 */
public enum ParameterType {
    INTEGER(Values.INTEGER),
    DOUBLE(Values.DOUBLE),
    FLOAT(Values.FLOAT),
    LINK(Values.LINK),
    STRING(Values.STRING);

    private ParameterType(String value) {
        if (!this.name().equals(value)) {
            throw new IllegalArgumentException("Incorrect use os ParameterType");
        }
    }

    /**
     * Static string values for the {@link ParameterType} due
     * http://stackoverflow.com/q/3639225/3989524, specifically answer http://stackoverflow.com/a/26719904/3989524
     */
    public static class Values {
        private Values() {}

        public static final String INTEGER = "INTEGER";
        public static final String DOUBLE = "DOUBLE";
        public static final String FLOAT = "FLOAT";
        public static final String LINK = "LINK";
        public static final String STRING = "STRING";
    }

}
