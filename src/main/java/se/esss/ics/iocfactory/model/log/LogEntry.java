/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model.log;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * A database table representing a log entry
 *
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Entity
@Table(name = "log_entry")
public class LogEntry implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "entry_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @Column(name = "entry_user", length = 40)
    private String user;

    @Column(name = "entry_type")
    @Enumerated(EnumType.STRING)
    private LogEntryType type;

    @Column(columnDefinition = "TEXT")
    private String details;

    public LogEntry() {}

    /**
     * Initializes a new log entry
     *
     * @param user
     * @param type
     * @param details
     */
    public LogEntry(String user, LogEntryType type, String details) {
        this.timestamp = new Date();
        this.user = user;
        this.type = type;
        this.details = details;
    }

    public Long getId() { return id; }

    public Date getTimestamp() { return timestamp != null ? new Date(timestamp.getTime()) : null; }
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp != null ? new Date(timestamp.getTime()) : null;
    }

    public String getUser() { return user; }
    public void setUser(String user) { this.user = user; }

    public LogEntryType getType() { return type; }
    public void setType(LogEntryType type) { this.type = type; }

    public String getDetails() { return details; }
    public void setDetails(String details) { this.details = details; }

    @Override
    public int hashCode() { return Objects.hashCode(this.id); }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LogEntry other = (LogEntry) obj;
        return Objects.equals(this.id, other.id);
    }
}
