/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import java.io.File;
import java.io.Serializable;

/**
 * Represents an repository scanning error message, has a path field in addition to the {@link ConsistencyStatusValue}
 * ones
 *
 * @author <a href=mailto:miroslav.pavleski@cosylab.com>Miroslav Pavleski</a>
 */
public class RepositoryMessage extends ConsistencyStatusValue
    implements ConsistencyStatus, Serializable {
    
    private String path;

    public RepositoryMessage(ConsistencyMsgLevel level, String desc, String path) {
        super(level, desc);
        this.path = path;
    }

    public RepositoryMessage(ConsistencyMsgLevel level, String desc, File path) {
        super(level, desc);
        this.path = path != null ? path.getAbsolutePath() : null;
    }

    public String getPath() { return path; }
    public void setPath(String path) { this.path = path; }
}
