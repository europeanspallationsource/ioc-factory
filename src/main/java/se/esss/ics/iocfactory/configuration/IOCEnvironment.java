/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.configuration;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Class to represent various IOC deployment environments such as development and production
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Entity
@Table(name = "iocenvironment")
public class IOCEnvironment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "env_name", unique = true)
    private String name;

    @Column(name = "directory")
    private String directory;

    @Column(name = "production")
    private boolean production = false;

    @Column(name = "eee_type")
    @Enumerated(value = EnumType.STRING)
    private EEEType eeeType;

    @Version
    @Column(name = "optimistic_version")
    private Long optimisticVersion;

    public Long getId() { return id; }

    /** Default constructor */
    public IOCEnvironment() {} // NOSONAR

    /**
     * Initialization constructor
     *
     * @param name name of the environment
     * @param directory directory on the share for this IOC environment
     * @param production if the environment is production
     * @param eeeType type of EEE environment to use with this environment
     */
    public IOCEnvironment(String name, String directory, boolean production, EEEType eeeType) {
        this.name = name;
        this.directory = directory;
        this.production = production;
        this.eeeType = eeeType;
    }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getDirectory() { return directory; }
    public void setDirectory(String directory) { this.directory = directory; }

    public boolean isProduction() { return production; }
    public void setProduction(boolean production) { this.production = production; }

    public EEEType getEeeType() { return eeeType; }
    public void setEeeType(EEEType eeeType) { this.eeeType = eeeType;}

    @Override
    public String toString() { return name; }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.name);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IOCEnvironment other = (IOCEnvironment) obj;
        return Objects.equals(this.name, other.name);
    }
}
