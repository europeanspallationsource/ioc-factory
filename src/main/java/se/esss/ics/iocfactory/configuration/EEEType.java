/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.configuration;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public enum EEEType {
    PRODUCTION("Production", "Production EEE"),
    SANDBOX("Sandbox", "Sandbox EEE");

    EEEType(String name, String userFriendlyName) {
        this.name = name;
        this.userFriendlyName = userFriendlyName;
    }

    private final String name;
    private final String userFriendlyName;

    public String getName() { return name; }
    public String getUserFriendlyName() { return userFriendlyName; }
}
