/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

/**
 * An abstract security interface for the applicaiton
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
public interface SecurityService {
    /**
     * Method that is to be invoked to login a user
     *
     * @param username User's username
     * @param password User's password
     * @return <code>true</code> if login is successful
     */
    public boolean doLogin(String username, String password);

    /**
     * Gets the username of the currently logged user if any
     *
     * @return the username
     */
    public String getUsername();

    /**
     * Method that is to be invoked to log an user out (logoff).
     */
    public void doLogout();

    /**
     * A method used to check whether user is logged in.
     *
     * @return <code>true</code> if user is logged in
     */
    public boolean isLoggedIn();


    /** @return true if the user can administer the ioc factory, else false; if no user logged in, false is returned */
    public boolean canAdminister();

    /** @return true if the user can configure ioc, else false; if no user logged in, false is returned */
    public boolean canConfigureIOCs();

    /** @return true if the user can deploy to non production*/
    public boolean canDeployNonProduction();

    /** @return true if the user can deploy to production*/
    public boolean canDeployProduction();
}
