/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service.verifiers;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.service.YamlHelper;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Stateless
public class ProcServVerifier extends ConfigVerifierBase implements ConfigVerifier {
    private static final Logger LOG = Logger.getLogger(ProcServVerifier.class.getName());
    private static final String VERIFIER_CATEGORY = "ProcServ";

    private static final YamlHelper<Configuration> YAML_HELPER = new YamlHelper<>(false);

    @Inject
    private transient MachineLevelVerifierUtil machineLevelUtil;

    @Override
    public int getPriority() { return 400; }
    
    @Override
    public String getCategory() { return VERIFIER_CATEGORY; }     


    @Override
    public void verifyImpl(final Configuration config, final Repository repo,  final IOCEnvironment env) {

        final int procServPort = config.getProcServPort();
        final String hostname = config.getIoc().getHostname();
            
        machineLevelUtil.otherGenerateEntriesStream(config, env).forEach(lastRevEntry -> {
            final JsonNode tree = YAML_HELPER.readTree(lastRevEntry.getDump());
            if (tree != null) {
                final JsonNode val = tree.findValue("procServPort");
                if (val.isInt() && val.asInt() == procServPort) {
                    LOG.warning(
                            addVerifyMessage(ConsistencyMsgLevel.WARN, "Configuration for %s revision %d on the same "
                                    + "hostname %s already uses PROCSERV_PORT value of %d", lastRevEntry.getIocName(),
                            lastRevEntry.getConfigRevision(), hostname, procServPort)
                    );
                }
            }
        });
    }
}
