/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service.verifiers;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.MacroExpansion;
import se.esss.ics.iocfactory.model.Parameter;
import se.esss.ics.iocfactory.model.PlaceholderTrait;
import se.esss.ics.iocfactory.model.Repository;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class MacroVerifier extends ConfigVerifierBase implements ConfigVerifier {

    private static final Logger LOG = Logger.getLogger(MacroVerifier.class.getName());
    private static final String VERIFIER_CATEGORY = "Macros";

    @Override
    public int getPriority() { return 300; }

    @Override
    public String getCategory() { return VERIFIER_CATEGORY; }     

    @Override
    public void verifyImpl(final Configuration config, final Repository repo, final IOCEnvironment env) {
        final Multimap<String, Parameter> allParams = ArrayListMultimap.create();
        final List<Parameter> paramsWithUniqueness = new ArrayList<>();
        
        // Handle macro error messages
        paramsStreamFromConfig(config).forEach(global -> addMacroMsg(global, allParams, paramsWithUniqueness));

        // Handle unique parameters
        final Set<String> processed = new HashSet<>();
        for (final Parameter param : paramsWithUniqueness) {
            if (!processed.contains(param.getName())) {
                processed.add(param.getName());

                final Collection<Parameter> matchedParams = allParams.get(param.getName());
                final Set<String> values = new HashSet<>();
                
                int nonEmptyParams = 0;
                for (final Parameter matchedParam : matchedParams) {
                    if (!StringUtils.isEmpty(matchedParam.getValue())) {
                        nonEmptyParams++;
                        values.add(matchedParam.getValue());
                    }
                }
                if (values.size() != nonEmptyParams) {
                    addUniqueMsg(param, matchedParams);
                } 
            }
        }
    }
    
    public static Stream<Parameter> paramsStreamFromConfig(final Configuration config) {
        return Stream.concat(config.getGlobals().stream(), 
                config.getDevices().stream().filter(dev -> !dev.getConsistencyStatus().isError()).
                        flatMap(dev -> dev.getParameters().stream()));
    }

    private void addMacroMsg(final Parameter param, final Multimap<String, Parameter> allMacros, 
            final List<Parameter> paramsWithUniqueness) {
        allMacros.put(param.getName(), param);

        final Set<PlaceholderTrait> paramTraits = param.getTraits();
        if (paramTraits != null && (paramTraits.contains(PlaceholderTrait.UNIQUE_IOC))) {
            paramsWithUniqueness.add(param);
        }

        // Handle statuses
        param.getConsistencyStatus().stream().filter(cs -> cs.getLevel() == ConsistencyMsgLevel.ERROR
                || cs.getLevel() == ConsistencyMsgLevel.WARN).forEach(cs -> {
            addVerifyMessage(cs.getLevel(), "%s: %s", getParamDesc(param), cs.getDesc());
        });

        // Handle expansion
        if (!param.getExpandedValues().isEmpty()) {
            int i = 0;
            for (final MacroExpansion expValue : param.getExpandedValues()) {
                i++;
                if (expValue.isError()) {                    
                    addVerifyMessage(ConsistencyMsgLevel.ERROR,
                            "%s, occurence %d - %s%s", getParamDesc(param), i, expValue.getError(),
                                    expValue.getOccurrence() != null ? String.format(" [%d, %d]",
                                    expValue.getOccurrence().getPosition().getLine(),
                                    expValue.getOccurrence().getPosition().getColumn()) : "");
                }
                if (StringUtils.isEmpty(expValue.getExpandedValue())) {
                    addMacroNotDefined(i, param);
                }
            }
        } else {
            addMacroNotDefined(null, param);
        }
    }

    private void addMacroNotDefined(Integer i, final Parameter param) {
        addVerifyMessage(ConsistencyMsgLevel.WARN, "%s%s - %s", getParamDesc(param),
                i != null ? String.format(", occurence %d", i) : "",
                "Macro not defined");
    }

    private void addUniqueMsg(Parameter param, Collection<Parameter> offending) {
        addVerifyMessage(ConsistencyMsgLevel.ERROR, "IOC-unique macro %s does not have unique value in the IOC. "
                + "Values are: %s", getParamDesc(param),
                offending.stream().map(p -> String.format("\"%s\"", p.getValue())).collect(Collectors.joining("; ")));
    }

    private static String getParamDesc(final Parameter param) {
        return String.format("%s in %s%s", param.getName(), param.getDeviceName(),
                param.getSnippetName() != null ? ", " + param.getSnippetName() : "");
    }
}
