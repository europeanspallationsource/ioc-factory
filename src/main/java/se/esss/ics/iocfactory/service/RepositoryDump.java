/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.output.NullWriter;
import org.apache.commons.lang3.ObjectUtils;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import se.esss.ics.iocfactory.model.Module;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.model.RepositoryStats;
import se.esss.ics.iocfactory.model.VersionConverters;
import se.esss.ics.iocfactory.util.Util;

/**
 * Dumps a repository structure in a tabbed text format
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
public class RepositoryDump {
    private static final Logger LOG = Logger.getLogger(RepositoryDump.class.getName());

    private RepositoryDump() {}

    public static void dumpRepositoryStats(final Repository repo, final RepositoryStats stats) {
        stats.setRepoTree(RepositoryDump.repositoryTree(repo));

        final ByteArrayOutputStream bos = new ByteArrayOutputStream(4096);
        try (PrintWriter writer = new PrintWriter(new OutputStreamWriter(bos, StandardCharsets.US_ASCII), false)) {
            stats.setRepoHash(RepositoryDump.dumpRepository(repo, writer));
            writer.flush();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            throw new RuntimeException("Writing repo dump failed", e);
        }
        stats.setRepoDump( bos.toByteArray() );
        stats.setRepoDumpStr( new String(stats.getRepoDump(), StandardCharsets.US_ASCII) );

        stats.setLastRepoScanTime( repo.getLastScanTime() );
    }

    public static String computeRepositoryHash(final Repository repo) {
        return RepositoryDump.dumpRepository(repo, new NullWriter());
    }

    private static String dumpRepository(Repository repo, Writer writer) {
        final MessageDigest digest = Util.getSHA1MessageDigest();

        repo.getEpicsVersions().stream().sorted().forEach(epicsVersion -> {
            dumpAndDigest(writer, digest, 0, "EPICS Version: %s\n",
                    VersionConverters.getEpicsConverter().toString(epicsVersion));
            repo.getOsVersions().stream().sorted().forEach(os -> {
                dumpAndDigest(writer, digest, 1, "OS: %s\n", os);
                repo.getModuleNames().stream().sorted().forEach(moduleName -> {
                    dumpAndDigest(writer, digest, 2, "%s: \n", moduleName);

                    repo.getModulesForSpec(moduleName, os, epicsVersion).stream().
                        sorted((l,r) -> ObjectUtils.compare(l.getModuleVersion(), r.getModuleVersion())).
                        forEach(module -> dumpModule(writer, digest, module));

                });
            });
        });
        return Hex.encodeHexString(digest.digest());
    }

    private static void dumpModule(Writer writer, MessageDigest digest, Module module) {
        dumpAndDigest(writer, digest, 3, "%s\n", module.getModuleVersion());

        final String deps = module.getDependencies().stream().
                sorted((l,r) -> ObjectUtils.compare(l.getName(), r.getName())).map(Object::toString).
                collect(Collectors.joining(", "));

        final String snippets =  module.getSnippets().stream().
                sorted((l,r) -> ObjectUtils.compare(l.getName(), r.getName())).
                map(snippet -> String.format("%s - %s", snippet.getName() ,
                        snippet.getPlaceholders().stream().
                                sorted((l,r) -> ObjectUtils.compare(l.getName(), r.getName())).
                                map(Object::toString).collect(Collectors.joining(", ")))).
                collect(Collectors.joining("; "));

        dumpAndDigest(writer, digest, 4, "Deps: %s\n", deps);
        dumpAndDigest(writer, digest, 4, "Snippets: %s\n",snippets);
    }

    private static void dumpAndDigest(Writer writer, MessageDigest digest, int level, String format, Object ... args) {
        final String str = identLevel(level) + String.format(format, args);

        digest.update(str.getBytes(StandardCharsets.US_ASCII));
        try {
            writer.append(str);
        } catch (IOException e) {
            throw new RuntimeException("Writing repository dump failed", e);
        }
    }

    private static String identLevel(int level) {
        switch (level) {
        case 0:
            return "";
        case 1:
            return "  ";
        case 2:
            return "    ";
        case 3:
            return "      ";
        case 4:
            return "        ";
        default:
            return "";
        }
    }


    private static TreeNode repositoryTree(Repository repo) {
        TreeNode root = new DefaultTreeNode();

        repo.getEpicsVersions().stream().sorted().forEach(epicsVersion -> {

            TreeNode epicsNode = addNode(root, 0, "EPICS Version: %s\n",
                    VersionConverters.getEpicsConverter().toString(epicsVersion));
            repo.getOsVersions().stream().sorted().forEach(os -> {
                TreeNode osNode = addNode(epicsNode, 1, "OS: %s\n", os);
                repo.getModuleNames().stream().sorted().forEach(moduleName -> {
                    TreeNode moduleNode = addNode(osNode, 2, "%s: \n", moduleName);

                    repo.getModulesForSpec(moduleName, os, epicsVersion).stream().
                            sorted((l, r) -> ObjectUtils.compare(l.getModuleVersion(), r.getModuleVersion())).
                            forEach(module -> moduleTree(moduleNode, module));

                });
            });
        });
        return root;
    }

    private static TreeNode addNode(TreeNode parent, int level, String format, Object ... args) {
        final String str = String.format(format, args);
        TreeNode node = new DefaultTreeNode(str, parent);
        node.setExpanded(false);
        return node;
    }

    private static void moduleTree(TreeNode parent, Module module) {
        TreeNode moduleVersionNode = addNode(parent, 3, "%s\n", module.getModuleVersion());
        moduleVersionNode.setExpanded(false);

        final String deps = module.getDependencies().stream().
                sorted((l,r) -> ObjectUtils.compare(l.getName(), r.getName())).map(Object::toString).
                collect(Collectors.joining(", "));

        final String snippets =  module.getSnippets().stream().
                sorted((l,r) -> ObjectUtils.compare(l.getName(), r.getName())).
                map(snippet -> String.format("%s - %s", snippet.getName() ,
                        snippet.getPlaceholders().stream().
                                sorted((l,r) -> ObjectUtils.compare(l.getName(), r.getName())).
                                map(Object::toString).collect(Collectors.joining(", ")))).
                collect(Collectors.joining("; "));

        addNode(moduleVersionNode, 4, "Deps: %s\n", deps);
        addNode(moduleVersionNode, 4, "Snippets: %s\n",snippets);
    }
}
