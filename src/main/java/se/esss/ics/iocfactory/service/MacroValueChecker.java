/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import se.esss.ics.iocfactory.model.MacroExpansion;
import se.esss.ics.iocfactory.model.Parameter;
import se.esss.ics.macrolib.MacroOccurrence;

/**
 * Provides methods for type and bounds checking for expanded macro values
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class MacroValueChecker {
    private static final String DOUBLE_CONVERSION_MESSAGE = "Cannot convert the parameter to DOUBLE";
    private static final String FLOAT_CONVERSION_MESSAGE = "Cannot convert the parameter to FLOAT";
    private static final String INTEGER_CONVERSION_MESSAGE = "Cannot convert the parameter to INTEGER";

    private static final Integer LINK_FIELD_SIZE = 80;
    private static final Integer STRING_FIELD_SIZE = 40;

    private static final String LINK_CONVERSION_MESSAGE = String.format("LINK parameters can be at most %d "
            + "characters long", LINK_FIELD_SIZE);

    private static final String STRING_CONVERSION_MESSAGE = String.format("STRING parameters can be at most %d "
            + "characters long", STRING_FIELD_SIZE);

    private MacroValueChecker() {}
    /**
     * Checks a value against the parameter type (either for conversion or character size limits). If no problem is
     * found returns {@code null}
     *
     * @param param
     * @param value
     * @param occurrence
     * @return null if the check is ok or a {@link MacroExpansion} containing the error message
     */
    public static MacroExpansion checkParameterValue(Parameter param, String value, MacroOccurrence occurrence) {
        switch (param.getType()) {
            case DOUBLE: return parseDouble(value, occurrence);
            case FLOAT: return parseFloat(value, occurrence);
            case INTEGER: return parseInteger(value, occurrence);
            case LINK:
                if (value.length() > LINK_FIELD_SIZE) {
                    return new MacroExpansion(value, occurrence, LINK_CONVERSION_MESSAGE);
                }
                break;
            case STRING:
                if (value.length() > STRING_FIELD_SIZE) {
                    return new MacroExpansion(value, occurrence, STRING_CONVERSION_MESSAGE);
                }
                break;
            default:
                throw new IllegalArgumentException(String.format("Unknown parameter type in the %s parameter checker",
                    MacroValueChecker.class.getCanonicalName()));
        }
        return null;
    }

    private static MacroExpansion parseDouble(String value, MacroOccurrence occurrence) {
        try {
            Double.parseDouble(value);
        } catch (NumberFormatException e) {
            return new MacroExpansion(value, occurrence, DOUBLE_CONVERSION_MESSAGE);
        }
        return null;
    }

    private static MacroExpansion parseFloat(String value, MacroOccurrence occurrence) {
        try {
            Float.parseFloat(value);
        } catch (NumberFormatException e) {
            return new MacroExpansion(value, occurrence, FLOAT_CONVERSION_MESSAGE);
        }
        return null;
    }

    private static MacroExpansion parseInteger(String value, MacroOccurrence occurrence) {
        try {
            Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return new MacroExpansion(value, occurrence, INTEGER_CONVERSION_MESSAGE);
        }
        return null;
    }
}
