/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.tuple.Pair;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.Module;
import se.esss.ics.iocfactory.model.ModuleDependency;
import se.esss.ics.iocfactory.model.ParameterPlaceholder;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.model.RepositoryMessage;
import se.esss.ics.iocfactory.model.Snippet;
import se.esss.ics.iocfactory.model.VersionConverters;
import se.esss.ics.iocfactory.util.Util;
import se.esss.ics.macrolib.MacroStorage;

/**
 * Scans a directory representing an EPICS module base for modules, OS versions and EPICS versions
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
public class RepositoryScanner {
    private static final Logger LOG = Logger.getLogger(RepositoryScanner.class.getCanonicalName());

    private static final String EPICS_BASES_DIR = "bases";
    private static final String EPICS_MODULES_DIR = "modules";
    private static final String EPICS_DIR_PREFIX = "base-";
    private static final String DEP_FILE_EXTENSION = ".dep";
    private static final String LIB_DIRECTORY = "lib";

    private static final Pattern SNIPPET_PATTERN = Pattern.compile("(.*)\\.cmd");

    /** a set of names that can occur in base/lib directory that are not in-fact OS version */
    private static final Set<String> BASE_LIB_OS_BLACK_LIST = Sets.newHashSet(
                "perl",
                "pkgconfig"
            );

    private final File repoDir;

    /**
     * Constructs a repo. scanner
     *
     * @param baseDir EPICS deployment base directory
     */
    public RepositoryScanner(File baseDir) {
        this.repoDir = baseDir;
    }


    /**
     * Scans the repository
     *
     * We use references and clearing of a repo because of CDI keeping the same reference, it is difficult to replace
     * it, once a Repository object is created by a producer method in the appropriate CDI scope
     *
     * @param inRepo optional existing repository reference
     * @return A {@link Repository} model object, if repo param was non-null it will hold the same reference
     */
    public Repository scanRepository(final Repository inRepo) {
        final Repository repo;
        if (inRepo == null) {
            repo = new Repository();
        } else {
            repo = inRepo;
            repo.clear();
        }
        repo.setLastScanTime(new Date());

        if (!isValidDir(repo, repoDir, "EPICS repository directory is invalid (does not exist). "
                + "Will have empty repo.")) {
            return repo;
        } else {
            scanEpicsBaseVersions(repo);
            scanOSVersions(repo);
            scanModules(repo);

            return repo;
        }
    }

    private void scanModules(Repository repo) {
        final File moduleDir = new File(repoDir, EPICS_MODULES_DIR);
        
        if (!isValidDir(repo, moduleDir, "Module repository directory is invalid (does not exist). "
                + "Will have empty repo.")) {
            return;
        } else {
            Arrays.stream(moduleDir.listFiles()).
                    filter(file -> file.isDirectory()).
                    forEach(dir
                            -> Arrays.stream(dir.listFiles()).
                            filter(verDir -> verDir.isDirectory()).
                            forEach(verDir -> {
                                final String verString = verDir.getName();
                                scanModule(repo, dir.getName(), verString, verDir);
                            }));
        }
    }


    private void scanModule(Repository repo, String moduleName, String moduleVersion, File moduleDir) {
        final Pattern epicsVerPattern = VersionConverters.EPICS_PATTERN;

        // Go trough each EPICS version directory. ToDo Check if EPICS version exists in scanned versions
        Arrays.stream(moduleDir.listFiles()).
            filter(epicsDir -> epicsDir.isDirectory() && epicsVerPattern.matcher(epicsDir.getName()).matches()).
                forEach(epicsDir -> {
                    final File libDir = new File(epicsDir, LIB_DIRECTORY);
                    if (!Util.testIfReadableDir(libDir)) {
                        logWarning(repo, "Directory not valid or does not exist", libDir);
                    } else {
                        // Go trough each OS directory
                        Arrays.stream(libDir.listFiles()).
                                filter(osDir -> osDir.isDirectory()).forEach(osDir -> {
                                    final File depFile = new File(osDir, moduleName + DEP_FILE_EXTENSION);
                                    final List<ModuleDependency> dependencies =
                                            ModuleDependencyLoader.loadModuleDependencies(repo, depFile);
                                    List<Snippet> snippets = loadSnippets(repo, new File(moduleDir, "startup"));
                                    final Module module = new Module(moduleName, moduleVersion,
                                            VersionConverters.getEpicsConverter().fromString(epicsDir.getName()),
                                            osDir.getName(), dependencies, snippets);
                                    repo.addModule(module);
                                });
                    }
                });
    }

    private boolean isValidDir(Repository repo, File dir, String message) {
        boolean rv = dir!=null && dir.exists() && dir.isDirectory();
        if (!rv) {
            logError(repo, message, dir);
        }
        return rv;
    }

    private void scanEpicsBaseVersions(Repository repo) {
        final File basesDir = new File(repoDir, EPICS_BASES_DIR);
        if (!isValidDir(repo, basesDir, "EPICS Bases dir does not exist")) {
            return;
        }

        Arrays.stream(basesDir.listFiles()).
            filter(f -> f.isDirectory()).
            map(f -> f.getName()).
            filter(dirName -> dirName.startsWith(EPICS_DIR_PREFIX)).
            forEach( dirName ->
                repo.addEPICSVersion(VersionConverters.getEpicsConverter().fromString(
                        dirName.substring(EPICS_DIR_PREFIX.length()))));
    }

    private void scanOSVersions(Repository repo) {
        Arrays.stream(repoDir.listFiles()).
            filter(f -> f.isDirectory() && f.getName().startsWith(EPICS_DIR_PREFIX)).
            map(f -> new File(f,"lib")).
            filter(Util::testIfReadableDir).
            forEach(libDir ->
                Arrays.stream(libDir.listFiles()).
                    filter(osDir -> osDir.isDirectory() && !BASE_LIB_OS_BLACK_LIST.contains(osDir.getName())).
                    forEach(osDir ->
                       repo.addOsVersion(osDir.getName())
                    )
            );
    }

    private List<Snippet> loadSnippets(Repository repo, File startupDir) {
        if (!Util.testIfReadableDir(startupDir)) {
            return ImmutableList.of();
        }

        final File[] fileList = startupDir.listFiles();
        if (fileList == null) {
            return ImmutableList.of();
        }

        final ArrayList<Snippet> snippets = new ArrayList<>();
        boolean anythingFound = false;

        for (File sf : fileList) {
            final Matcher matcher = SNIPPET_PATTERN.matcher(sf.getName());
            if (matcher.matches() && matcher.groupCount()==1) {
                anythingFound = true;

                final Pair<MacroStorage, List<ParameterPlaceholder>> lm =
                        (new MacroLoader(repo, sf)).loadMacros();
                snippets.add( new Snippet(matcher.group(1), sf, lm.getLeft(), lm.getRight()));
            }
        }

        if (!anythingFound) {
            logWarning(repo, String.format("No snippets found matching the pattern %s in the directory, "
                    + "is the pattern valid?", SNIPPET_PATTERN.toString()), startupDir);
        }

        return snippets;
    }

    private void logError(Repository repo, String message, File dir) {
        LOG.severe(message);
        repo.getMessages().add(new RepositoryMessage(ConsistencyMsgLevel.ERROR, message, dir));
        repo.setError(true);
    }

    private void logWarning(Repository repo, String message, File dir) {
        LOG.fine(message);
        repo.getMessages().add(new RepositoryMessage(ConsistencyMsgLevel.WARN, message, dir));
    }
}
