package se.esss.ics.iocfactory.ui;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 * Manager for loading properties required by the UI.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Named
@ViewScoped
public class UIPropertiesManager implements Serializable {

    private static final long serialVersionUID = -8496869471465056456L;
    public static final String SUPPORT_EMAIL_PROPERTY = "iocfactory.supportEmail";
    private static final Logger LOGGER = Logger.getLogger(UIPropertiesManager.class.getCanonicalName());

    /** @return the support email */
    public String getSupportEmail() {
        final String supportEmail = System.getProperty(SUPPORT_EMAIL_PROPERTY, null);
        LOGGER.log(Level.FINE, "Support email: " + supportEmail);
        return supportEmail;
    }
}
