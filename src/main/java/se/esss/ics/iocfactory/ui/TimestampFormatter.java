/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import java.util.Date;
import javax.inject.Named;
import org.apache.commons.lang3.time.FastDateFormat;

/**
 * A utility class to format timestamps up to a seccond
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Named
public class TimestampFormatter {
    private static final FastDateFormat dateFormat = FastDateFormat.getInstance("yyyy.MM.dd HH:mm:ss");

    private TimestampFormatter() {}

    public static String format(Date date) {
        return date != null ? dateFormat.format(date) : "";
    }
}
