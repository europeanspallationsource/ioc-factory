/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import org.omnifaces.util.Faces;
import org.primefaces.context.RequestContext;
import se.esss.ics.iocfactory.service.RepositoryProducer;
import se.esss.ics.iocfactory.util.Util;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Manages configuration saving when switching between menus, repo refresh and logout.
 */
@Named
@ViewScoped
public class NavigationManager implements Serializable {

    private static final Logger LOG = Logger.getLogger(NavigationManager.class.getName());

    @Inject
    private DetailsManager detailsManager;

    @Inject
    private LoginManager loginManager;

    @Inject
    private RepositoryProducer repositoryProducer;

    @Inject
    private RepositorySelector repositorySelector;

    /** Target page or action to run. */
    private String targetPage = "";

    /**
     * If there were changes to configuration, prompts the user for desired action, or just do the redirection/action.
     * @param page target page or action on page to be run.
     *             Supports page name, "logout" or "refresh".
     */
    public void fireNavigation(final String page) {
        // remember where to go/what to do
        targetPage = page;
        if (detailsManager.isDirty()) {
            // if navigating to the same page, cancel navigation silently,
            // if to other page, display the dialog
            if (!"/configure.xhtml".equals(targetPage)) {
                RequestContext.getCurrentInstance().execute("handleNavigation();");
            }
        } else {
            completeNavigation();
        }
    }

    /** Saves configuration and completes navigation. */
    public void saveConfiguration() {
        detailsManager.saveConfiguration(false);
        completeNavigation();
    }

    /** Discards configuration and completes navigation. */
    public void discardConfiguration() {
        detailsManager.discardConfiguration(false);
        completeNavigation();
    }

    private void completeNavigation() {
        switch (targetPage) {
        case "":
            // no action
            break;
        case "logout":
            // logout
            try {
                loginManager.onLogout();
                Faces.refresh();
            } catch (IOException e) {
                LOG.log(Level.INFO, "Could not refresh page after logout", e);
                Util.addGlobalInfo("Could not refresh page after logout.");
            }
            break;
        case "refresh":
            // repository refresh
            try {
                repositoryProducer.refreshSessionRepository(repositorySelector.getSelectedEee());
                Faces.refresh();
            } catch (IOException e) {
                LOG.log(Level.SEVERE, "Could not refresh page", e);
                Util.addGlobalError("Could not refresh page.");
            }
            break;
        case "repoSelectLabel":
            // repo select link
            try {
                final String requestURI = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                        .getRequest()).getRequestURI();

                // split by page name - to get the root path
                String[] splitStrings = requestURI.split("/[^/]*\\.xhtml");

                final String targetUrl = String.format("%s/setup.xhtml?showmessages=%s",
                        splitStrings[0],
                        repositorySelector.getSelectedEee().getName());
                Faces.redirect(targetUrl);
            } catch (IOException e) {
                LOG.log(Level.INFO, "Could not open EEE Scanning Messages page", e);
                Util.addGlobalInfo("Could not open EEE Scanning Messages page.");
            }
            break;
        default:
            // page (menu) navigation
            Faces.navigate(targetPage);
            break;
        }
        // reset for next use
        targetPage = "";
    }
}
