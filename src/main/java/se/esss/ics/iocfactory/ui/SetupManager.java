/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import com.google.common.collect.ImmutableList;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.configuration.IOCFactSetup;
import se.esss.ics.iocfactory.service.SecurityService;
import se.esss.ics.iocfactory.service.SetupService;
import se.esss.ics.iocfactory.util.Util;

/**
 * A JSF backing bean for handling the Setup screen general options
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Named
@ViewScoped
public class SetupManager implements Serializable {
    private static final Logger LOG = Logger.getLogger(SetupManager.class.getCanonicalName());

    @Inject private transient SetupService setupService;
    @Inject private transient SecurityService securityService;

    private boolean canAdminister;
    private IOCFactSetup setup;
    private List<IOCEnvironment> selectedEnvs = ImmutableList.of();

    /**
     * Initialize the bean
     */
    @PostConstruct
    public void init() {
        canAdminister = securityService.canAdminister();
        setup = setupService.loadSetup();
    }

    /**
     * Getter for the {@link IOCFactSetup} setup object
     * @return the setup object
     */
    public IOCFactSetup getSetup() { return setup; }

    /**
     * Handles a click to the "Save" button on the Setup screen
     */
    public void saveConfiguration() {
        Util.callSecuredEJB(LOG, () -> {
            setupService.saveSetup(setup);
        }).ifSuccessful(() -> {
            // Reload othewise Optimistic lock exception
            setup = setupService.loadSetup();
            Util.addGlobalInfo("IOC Factory Configuration saved.");
        });
    }

    public List<IOCEnvironment> getSelectedEnvs() { return selectedEnvs; }
    public void setSelectedEnvs(List<IOCEnvironment> selectedEnvs) { this.selectedEnvs = selectedEnvs; }

    public boolean isEditButtonEnabled() { return selectedEnvs!=null && selectedEnvs.size()==1; }
    public boolean isDeleteButtonEnabled() { return selectedEnvs!=null && !selectedEnvs.isEmpty(); }

    public boolean canAdminister() { return canAdminister; }
}
