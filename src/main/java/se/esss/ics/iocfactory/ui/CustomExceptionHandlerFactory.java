package se.esss.ics.iocfactory.ui;

import org.omnifaces.exceptionhandler.FullAjaxExceptionHandlerFactory;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

/**
 * A factory for the CustomExceptionHandler.
 */
public class CustomExceptionHandlerFactory extends FullAjaxExceptionHandlerFactory {

    /**
     * A new JSF exception handler factory
     * @param wrapped the original JSF exception handler factory
     */
    public CustomExceptionHandlerFactory(ExceptionHandlerFactory wrapped) {
        super(wrapped);
    }

    @Override
    public ExceptionHandler getExceptionHandler() {
        return new CustomExceptionHandler(super.getWrapped().getExceptionHandler());
    }

}
