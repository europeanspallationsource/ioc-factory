/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 * A backing bean providing data for the dual (env.sh and st.cmd) compare
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Named("dualCompare")
@ViewScoped
public class DualCompareConfigManager implements Serializable {
    private final List<String> envs = Lists.newArrayList("", "");
    private final List<String> sts = Lists.newArrayList("", "");
    private final List<String> titles = Lists.newArrayList("", "");


    public List<String> getTitles() { return titles; }
    public List<String> getEnvs() { return envs; }
    public List<String> getSts() { return sts; }
}
