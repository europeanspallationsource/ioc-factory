/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import com.google.common.collect.ImmutableList;
import java.io.Serializable;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.StringUtils;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.Parameter;
import se.esss.ics.iocfactory.model.ParameterPlaceholder;
import se.esss.ics.iocfactory.model.ParameterType;
import se.esss.ics.iocfactory.util.Util;

import static se.esss.ics.iocfactory.model.PlaceholderTrait.*;
import se.esss.ics.iocfactory.model.Snippet;

/**
 * A JSF backing bean for handling addition or deletion of Globals for a configuration
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Named
@ViewScoped
public class GlobalsManager implements Serializable {
    @Inject private DetailsManager detailsManager;

    private Configuration config;
    private List<ParameterPlaceholder> globalHints = Collections.emptyList();

    private String macroName = "";
    private String macroValue = "";

    private Parameter editedParam;

    /** Explicitly initializes the bean */
    public void prepareAdd() {
        getAndCheckConfig();
        macroName = "";
        macroValue = "";
        editedParam = null;
    }

    /**
     * Explicitly initializes the bean for a parameter to be edited
     *
     * @param param the global parameter to be edited
     */
    public void prepareEdit(Parameter param) {
        getAndCheckConfig();
        editedParam = param;
        macroName = param.getName();
        macroValue = param.getValue();
    }

    /** Handles the "Add" global macro button */
    public void add() {
        if (config != null && validateMacroName("addGlobalForm")) {
            config.getGlobals().add(createNewParameter());
            detailsManager.makeConfigDirty();
            detailsManager.renumerateAndPrepareConfig();
        }
    }

    /** Handles the "Edit" global macro button */
    public void edit()  {
        if (config != null && validateMacroName("editGlobalForm")) {
            final int origIdx = config.getGlobals().indexOf(editedParam);
            config.getGlobals().set(origIdx, createNewParameter());
            detailsManager.setSelectedGlobals(ImmutableList.of(config.getGlobals().get(origIdx)));
            detailsManager.makeConfigDirty();
            detailsManager.renumerateAndPrepareConfig();
        }
    }

    public List<ParameterPlaceholder> getGlobalsHints() { return globalHints; }

    public String getMacroName() { return macroName; }
    public void setMacroName(String paramName) { this.macroName = paramName; }

    public String getMacroValue() { return macroValue; }
    public void setMacroValue(String macroValue) { this.macroValue = macroValue; }

    public String getMacroDesc() {
        final Optional<ParameterPlaceholder> placeholder = findPlaceholderByName(macroName);
        return placeholder.isPresent() ? placeholder.get().getDesc() : "";
    }

    public ParameterType getMacroType() {
        final Optional<ParameterPlaceholder> placeholder = findPlaceholderByName(macroName);
        return placeholder.isPresent() ? placeholder.get().getType() : ParameterType.STRING;
    }

    public void emptyListener() {}

    private Parameter createNewParameter() {
        final ParameterPlaceholder placeholder = findPlaceholderByName(macroName).
                orElse(new ParameterPlaceholder(macroName, ParameterType.STRING, "", EnumSet.of(PLAIN)));
        final Parameter param = new Parameter(placeholder, null);
        param.setValue(macroValue);
        return param;
    }

    private boolean validateMacroName(String formName) {
        if (StringUtils.isEmpty(macroName)) {
            Util.invalidateComponent(formName + ":macroName", "Please provide macro name");
            return false;
        } else if (config.getGlobals().stream().anyMatch(global -> global != editedParam &&
                Objects.equals(macroName, global.getName()))) {
            Util.invalidateComponent(formName + ":macroName", "Environment macro with that name is already in the "
                    + "configuration");
            return false;
        }
        return true;
    }

    private Optional<ParameterPlaceholder> findPlaceholderByName(String name) {
        return globalHints.stream().filter(p -> Objects.equals(name, p.getName())).findFirst();
    }

    private void getAndCheckConfig() {
        globalHints = Collections.emptyList();

        config = detailsManager.getConfig();
        if (config == null) {
            Util.addGlobalWarn("No configuration selected to modify environment macros");
        } else if (config.isCommited()) {
            Util.addGlobalWarn("Cannot modify environemnt macros for a commited configuration");
            config = null;
        } else {
            globalHints = config.getDevices().stream().flatMap(dev -> {
                if (dev == null || dev.getSnippets()== null) {
                    return Stream.empty();
                } else {
                    return dev.getSnippets().stream().flatMap(sn -> getGlobalHintsFromSnippet(sn));
                }
            }).distinct().collect(Collectors.toList());
        }
    }
    
    private static Stream<ParameterPlaceholder> getGlobalHintsFromSnippet(Snippet sn) {
        return sn.getPlaceholders().stream().filter(p -> p.getTraits().contains(GLOBAL_HINT));
    }
}
