/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import org.apache.commons.lang3.mutable.MutableInt;
import se.esss.ics.iocfactory.model.Configuration;

/**
 * Provides UI helper functions for manipulating {@link Configuration} objects
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class ConfigurationUtil {
    private static final int SUFFICENTLY_MAX_NUM_OF_GLOBALS = 10000;

    private ConfigurationUtil() {}

    /**
     * Prepares a loaded configuration for editing by setting unique integer ids for devices & parameters
     * The unique ids are used in the UI to refer to the elements and are JPA transient
     *
     * @param config
     */
    public static void renumerateConfigEntities(Configuration config) {
        // Numerate Devices
        MutableInt counter = new MutableInt(0);
        config.getDevices().forEach(dev -> {
            dev.setDevicePosition(counter.intValue());
            counter.increment();
        });

        // Numerate globals
        counter.setValue(0);
        config.getGlobals().forEach(global -> {
           global.setParamPosition(counter.intValue());
           counter.increment();
        });

        // Numerate params
        counter.setValue(SUFFICENTLY_MAX_NUM_OF_GLOBALS);
        config.getDevices().stream().
                flatMap(dev -> dev.getParameters().stream()).forEach(param -> {
                   param.setParamPosition(counter.intValue());
                   counter.increment();
                });
    }
}
