package se.esss.ics.iocfactory.ui;

import java.net.URLEncoder;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJBException;
import javax.el.ELException;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;

import org.omnifaces.exceptionhandler.FullAjaxExceptionHandler;

/**
 * A global JSF exception handler that displays caught exceptions in the UI as
 * popup messages and logs them.
 */
public class CustomExceptionHandler extends FullAjaxExceptionHandler {

    private static final Logger LOGGER = Logger
            .getLogger(CustomExceptionHandler.class.getCanonicalName());
    private static final String UNEXPECTED_ERROR = "Unexpected error";
    private static final String SESSION_ERROR = "Session error";

    /**
     * A new JSF exception handler
     * 
     * @param wrapped
     *            the original JSF exception handler
     */
    public CustomExceptionHandler(ExceptionHandler wrapped) {
        super(wrapped);
    }

    @Override
    public void handle() throws FacesException {
        final Iterator<ExceptionQueuedEvent> unhandledExceptionQueuedEvents = getUnhandledExceptionQueuedEvents()
                .iterator();

        if (!unhandledExceptionQueuedEvents.hasNext()) {
            return;
            // there are no unhandled exceptions
        }

        final Throwable unwrappedException = unhandledExceptionQueuedEvents
                .next().getContext().getException();

        final Throwable throwable = getExceptionNonframeworkCause(
                unwrappedException);
        
        if (!(throwable instanceof javax.faces.application.ViewExpiredException)) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            UNEXPECTED_ERROR, throwable.getMessage()));
            LOGGER.log(Level.SEVERE, UNEXPECTED_ERROR, throwable);
        } else {
            super.handle();
        }
    }

    /*
     * Returns the nested exception cause that is not Faces or EJB exception, if
     * it exists.
     */
    private Throwable getExceptionNonframeworkCause(Throwable exception) {
        return (exception instanceof FacesException
                || exception instanceof EJBException
                || exception instanceof ELException)
                && (exception.getCause() != null)
                        ? getExceptionNonframeworkCause(exception.getCause())
                        : exception;
    }
}
