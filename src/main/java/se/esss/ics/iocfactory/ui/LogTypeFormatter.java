/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import javax.inject.Named;
import se.esss.ics.iocfactory.model.log.LogEntryType;

/**
 * Provides static method that formats a log entry type ({@link LogEntryType}) for UI display
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Named
public class LogTypeFormatter {
    private LogTypeFormatter() { }

    public String format(LogEntryType let) {
        switch (let) {
            case CONFIG_ADDED_V1: return "Configuration Added";
            case CONFIG_GENERATED_V1: return "Configuration Generated";
            case CONFIG_DELTED_V1: return "Configuration Deleted";
            case CONFIG_SAVED_V1: return "Configuration Updated";
            case SETTINGS_SAVED_V1: return "Settings Saved";
            default: throw new IllegalArgumentException("Unknown " + LogEntryType.class.getName() + " type");
        }
    }
}
