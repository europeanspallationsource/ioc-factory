/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import se.esss.ics.iocfactory.configuration.EEEType;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import se.esss.ics.iocfactory.service.SecurityService;
import se.esss.ics.iocfactory.service.Settings;
import se.esss.ics.iocfactory.ui.cdievents.EeeSwitched;

/**
 * Session scoped class which holds the selected EEE environment to use
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Named
@SessionScoped
public class RepositorySelector implements Serializable {
    private static final String REPO_SETTING_KEY = "Repository";
 private EEEType previousEee = null;

    @Inject
    private SecurityService securityService;

    @Inject
    @EeeSwitched
    private Event<EEEType> eeeSwitchEvent;

    @Inject
    private Settings settings;

    public EEEType getSelectedEee() {
        if (securityService.isLoggedIn()) {
            final String repoString = settings.getSetting(REPO_SETTING_KEY);
            if (repoString == null) {
               return getDefaultProductionRepo();
            } else {
                try {
                    return EEEType.valueOf(repoString);
                } catch (IllegalArgumentException e) {
                    return getDefaultProductionRepo();
                }
            }
        } else {
            return EEEType.PRODUCTION;
        }
    }

    public void setSelectedEee(final EEEType newEee) {
        final EEEType selectedEee = getSelectedEee();

        this.previousEee = selectedEee;
        settings.setSetting(REPO_SETTING_KEY, newEee.toString());
    }

    public EEEType[] getEeeTypes() { return EEEType.values(); }

    public void revertSelection() { setSelectedEee(previousEee); }

    public void fireEvent() { eeeSwitchEvent.fire(getSelectedEee()); }

    private EEEType getDefaultProductionRepo() {
        final EEEType selectedEee = EEEType.PRODUCTION;
        settings.setSetting(REPO_SETTING_KEY, selectedEee.toString());
        return selectedEee;
    }
}
