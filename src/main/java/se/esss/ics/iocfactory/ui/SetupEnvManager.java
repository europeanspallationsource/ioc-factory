/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import com.google.common.collect.ImmutableList;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.StringUtils;
import se.esss.ics.iocfactory.configuration.EEEType;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.service.SetupService;
import se.esss.ics.iocfactory.util.Util;

/**
 * A JSF backing bean for handling the Setup screen IOC Environments list
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Named
@ViewScoped
public class SetupEnvManager implements Serializable {
    private static final Logger LOG = Logger.getLogger(SetupEnvManager.class.getName());

    @Inject private transient SetupService setupService;

    private List<IOCEnvironment> envs = Collections.emptyList();
    private List<IOCEnvironment> selectedEnvs = Collections.emptyList();

    private IOCEnvironment editEnv = new IOCEnvironment();

    /**
     * Initialize the bean
     */
    @PostConstruct
    public void init() { envs = setupService.loadEnvs(); }

    public List<IOCEnvironment> getEnvs() { return envs; }

    public List<IOCEnvironment> getSelectedEnvs() { return selectedEnvs; }
    public void setSelectedEnvs(List<IOCEnvironment> selectedEnvs) { this.selectedEnvs = selectedEnvs; }

    public boolean isEditButtonEnabled() { return selectedEnvs!=null && selectedEnvs.size()==1; }
    public boolean isDeleteButtonEnabled() { return selectedEnvs!=null && !selectedEnvs.isEmpty(); }

    public IOCEnvironment getEditEnv() { return editEnv; }


    /** Prepares new env for editing */
    public void handleNewEnv() {
        editEnv = new IOCEnvironment();
        editEnv.setEeeType(EEEType.PRODUCTION);
    }

    /** Prepares selected env for editing */
    public void handleEditEnv() {
        if (selectedEnvs==null || selectedEnvs.size()!=1) {
            Util.addGlobalError("Only single environment could be selected for editing.");
            return;
        }

        editEnv = selectedEnvs.get(0);
    }

    /**
     * Handles the click on the "Add" button on the Add IOC environment dialog
     */
    public void addIOCEnvironment() {
        // Check for empty name (required on p:inputText does not work well with p:dialog, it validates on display
        if (StringUtils.isEmpty(editEnv.getName())) {
            Util.invalidateComponent("addEnvForm:envName", "Name is missing");
            return;
        }

        // Check if list already contains equally named environment
        if (envs.stream().
                anyMatch(iocEnv -> StringUtils.equals(iocEnv.getName(), editEnv.getName()))) {
            Util.invalidateComponent("addEnvForm:envName", "Name exists already");
            return;
        }

        Util.callSecuredEJB(LOG, () -> {
            setupService.addEnv(editEnv);
        }).ifSuccessful(() -> {
            // Reload ! (otherwise optimistic lock problems)
            envs = setupService.loadEnvs();

            selectedEnvs = ImmutableList.of();
        });
    }

    /** Handles the click on "Save" button when editing environment */
    public void saveIOCEnvironment() {
        // Check for empty name (required on p:inputText does not work well with p:dialog, it validates on display
        if (StringUtils.isEmpty(editEnv.getName())) {
            Util.invalidateComponent("editEnvForm:envName", "Name is missing");
            return;
        }

        // Check if some of the other environments has name clash
        for (int i=0;i<envs.size();i++) {
            if (envs.get(i) != editEnv && StringUtils.equals(envs.get(i).getName(), editEnv.getName())) {
                Util.invalidateComponent("editEnvForm:envName", "Name exists already");
                return;
            }
        }

        Util.callSecuredEJB(LOG, () -> {
            setupService.saveEnv(editEnv);
        }).ifSuccessful(() -> {
            // Reload ! (otherwise optimistic lock problems)
            envs = setupService.loadEnvs();
            selectedEnvs = ImmutableList.of();
        });
    }

    /**
     * Handles an {@link IOCEnvironment} removal from the UI list of IOC environments
     *
     */
    public void removeIOCEnvironment() {
        if (selectedEnvs==null || selectedEnvs.isEmpty()) {
            Util.addGlobalError("No IOC environment selected for removal.");
            return;
        }

        Util.callSecuredEJB(LOG, () -> {
            selectedEnvs.forEach(env -> setupService.removeEnv(env));
        }).ifSuccessful(() -> {
            // Reload ! (otherwise optimistic lock problems)
            envs = setupService.loadEnvs();
            selectedEnvs = ImmutableList.of();
        });
    }
}
