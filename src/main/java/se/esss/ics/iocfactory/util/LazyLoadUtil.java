/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;

import org.primefaces.model.SortOrder;

import se.esss.ics.iocfactory.service.IndependentSortOrder;

/**
 * Helper methods for lazy loading.
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
public class LazyLoadUtil {
    
    private LazyLoadUtil(){}
    
    /***
     * Check if filter value is not null, so toString operation can return value without NullPointerException. In case of null, null is returned.
     * 
     * @param filedName Name of the Primefaces datatable filter column
     * @param filters Map of filters from Primefaces lazy load data model
     * @return String of filter value
     */
    public static String getFilterValueToString(String filedName, Map<String, Object> filters) {
        return filters.get(filedName) == null ? null : filters.get(filedName).toString();
    }

    /***
     * Parses date in string to java.util.Date using StringToDateParser. If field value is null, null is returned.
     * 
     * @param filedName Name of the Primefaces datatable filter column
     * @param filters Map of filters from Primefaces lazy load data model
     * @return Date of filter value
     */
    public static Date getFilterValueToDate(String filedName, Map<String, Object> filters) {
        if (filters.get(filedName) == null) {
            return null;
        }        
        final LocalDateTime localTime = StringToDateParser.processUIDateTime(filters.get(filedName).toString());  
        final Date out = Date.from(localTime.atZone(ZoneId.of("UTC")).toInstant());
        return out;
    }

    /***
     * Escapes characters that are special chars for SQL like operations (%,_,*,\).
     * 
     * @param str input String
     * @return escaped String
     */
    public static String escapeSqlLikeChars(String str) {
        return str.replace("%", "\\%").replace("_", "\\_").replace("*", "\\*").replace("\\", "\\\\");
    }

    /***
     * Cast Primefaces sort order to own independent type.
     * 
     * @param sortOrder primefaces sorting Enum
     * @return independent sort order
     */
    public static IndependentSortOrder castFromPfSortOrder(SortOrder sortOrder) {
        if (sortOrder.equals(SortOrder.ASCENDING)) {
            return IndependentSortOrder.ASCENDING;
        }
        return IndependentSortOrder.DESCENDING;
    }
    
    /***
     * Add sort order to criteria API query.
     * 
     * @param user independent sort order
     * @param path path to table column
     * @param cb criteria builder context
     * @param gcq criteria query context
     */
    public static void addSortingToCaQuery(IndependentSortOrder user, Path<?> path, CriteriaBuilder cb, CriteriaQuery<?> gcq) {
        if (user == IndependentSortOrder.ASCENDING) {
            gcq.orderBy(cb.asc(path));
        }
        if (user == IndependentSortOrder.DESCENDING) {
            gcq.orderBy(cb.desc(path));
        }
    }
}
