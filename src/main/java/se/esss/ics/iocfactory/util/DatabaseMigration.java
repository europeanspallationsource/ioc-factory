package se.esss.ics.iocfactory.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.sql.DataSource;

import org.flywaydb.core.Flyway;

@Singleton
@Startup
@TransactionManagement(value = TransactionManagementType.BEAN)
public class DatabaseMigration {

    @Resource(lookup = "java:/se.esss.ics.iocfactory")
    private DataSource dataSource;

    @PostConstruct
    private void onStartup() {

        if (dataSource == null) {
            throw new EJBException("DataSource could not be found!");
        }

        Map<String, String> placeholders = new HashMap<>();
        System.getProperties().entrySet().forEach((e) -> {
            String key = (String) e.getKey();
            String value = (String) e.getValue();
            if (key.startsWith("flyway.placeholders.")) {
                placeholders.put(key.substring(20), value);
            }
        });

        Flyway flyway = new Flyway();
        flyway.setPlaceholders(placeholders);
        flyway.setDataSource(dataSource);
        flyway.migrate();
    }
}
